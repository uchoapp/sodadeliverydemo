package com.example.sodadelivery;

public interface BaseView {
    void showLoadingSpinner();
    void hideLoadingSpinner();
}