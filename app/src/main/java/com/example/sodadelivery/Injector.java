package com.example.sodadelivery;

import android.content.Context;

import com.example.library.DefaultExecutionScheduler;
import com.example.library.ExecutionScheduler;
import com.example.library.services.infile.InputStreamProvider;
import com.example.library.services.infile.InputStreamProviderImpl;
import com.example.library.services.infile.SodaApiServiceImpl;
import com.example.library.usecases.PurchaseSodaUseCaseImpl;
import com.example.library.usecases.RetrieveAllSodasUseCaseImpl;
import com.example.library.usecases.ShowSodaDetailsUseCaseImpl;
import com.example.sodadelivery.domain.services.SodaApiService;
import com.example.sodadelivery.domain.usecases.PurchaseSodaUseCase;
import com.example.sodadelivery.domain.usecases.RetrieveAllSodasUseCase;
import com.example.sodadelivery.domain.usecases.ShowSodaDetailsUseCase;
import com.example.sodadelivery.modules.details.SodaDetailsContract;
import com.example.sodadelivery.modules.details.SodaDetailsPresenter;
import com.example.sodadelivery.modules.list.SodaListContract;
import com.example.sodadelivery.modules.list.SodaListPresenter;

import java.util.HashMap;

public class Injector {

    private static HashMap<Class, Object> dependencies = new HashMap<>();

    public static ExecutionScheduler getDefaultExecutionScheduler() {
        return new DefaultExecutionScheduler();
    }

    public static InputStreamProvider getInputStreamProvider(Context context) {
        if(!hasDependency(InputStreamProvider.class))
            addDependency(InputStreamProvider.class, new InputStreamProviderImpl(context));

        return getDependency(InputStreamProvider.class);
    }

    public static SodaApiService getSodaApiService(Context context) {
        if(!hasDependency(SodaApiService.class))
            addDependency(SodaApiService.class, new SodaApiServiceImpl(getInputStreamProvider(context), getDefaultExecutionScheduler()));

        return getDependency(SodaApiService.class);
    }

    // List ----
    public static RetrieveAllSodasUseCase getRetrieveAllSodasUseCase(Context context) {
        return new RetrieveAllSodasUseCaseImpl(getDefaultExecutionScheduler(), getSodaApiService(context));
    }

    public static SodaListContract.Presenter getSodaListPresenter(Context context, SodaListContract.View view) {
        return new SodaListPresenter(getRetrieveAllSodasUseCase(context), view);
    }
    // List end ----

    // Details --
    public static ShowSodaDetailsUseCase getShowSodaDetailsUseCase(Context context) {
        return new ShowSodaDetailsUseCaseImpl(getDefaultExecutionScheduler(), getSodaApiService(context));
    }

    public static PurchaseSodaUseCase getPurchaseSodaUseCase(Context context) {
        return new PurchaseSodaUseCaseImpl(getDefaultExecutionScheduler(), getSodaApiService(context));
    }
    // ---

    public static SodaDetailsContract.Presenter getSodaDetailsPresenter(Context context, SodaDetailsContract.View view) {
        return new SodaDetailsPresenter(getShowSodaDetailsUseCase(context), getPurchaseSodaUseCase(context), view);
    }

    private static <T> void addDependency(Class<T> clazz, T object) {
        dependencies.put(clazz, object);
    }

    private static <T> boolean hasDependency(Class<T> clazz) {
        return dependencies.containsKey(clazz);
    }

    private static <T> T getDependency(Class<T> clazz) {
        return (T) dependencies.get(clazz);
    }
}
