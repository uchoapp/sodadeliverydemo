package com.example.sodadelivery.modules.details;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.sodadelivery.Injector;
import com.example.sodadelivery.R;
import com.example.sodadelivery.domain.dtos.SodaDto;

public class SodaDetailsActivity extends AppCompatActivity implements SodaDetailsContract.View {
    private static final int ANIM_DURATION = 500;
    public static final String EXTRA_SODA_ID = "sodaId";

    private ImageView backgroundImageView;
    private TextView sodaDescriptionTextView;
    private Button purchaseSodaButton;
    private ProgressBar progressBar;
    private View viewInterfaceLocker;

    private int sodaId;

    private SodaDetailsContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soda_details);
        setupToolbar();
        setupUI();

        sodaId = getSodaId();

        if (sodaId > 0) {
            presenter = Injector.getSodaDetailsPresenter(this, this);
            presenter.loadSoda(sodaId);
            setupPurchaseButton(sodaId);
        } else {
            Toast.makeText(this, R.string.soda_not_found, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_SODA_ID, sodaId);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        sodaId = savedInstanceState.getInt(EXTRA_SODA_ID, 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupUI() {
        backgroundImageView = (ImageView) findViewById(R.id.backgroundImageView);
        sodaDescriptionTextView = (TextView) findViewById(R.id.textViewSodaDescription);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        viewInterfaceLocker = findViewById(R.id.interfaceLocker);
        purchaseSodaButton = (Button) findViewById(R.id.purchaseButton);
    }

    private int getSodaId() {
        if (getIntent() == null) {
            return -1;
        }

        if (getIntent().getExtras() == null) {
            return -1;
        }

        return getIntent().getExtras().getInt(EXTRA_SODA_ID, -1);
    }

    private void setupPurchaseButton(final int sodaId) {
        purchaseSodaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.purchaseSoda(sodaId);
            }
        });
    }

    @Override
    public void showPurchaseFeedback(boolean purchaseResult) {
        if (purchaseResult) {
            Toast.makeText(this, getString(R.string.purchase_soda_succeeded), Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, getString(R.string.purchase_soda_failed), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showDetails(SodaDto dto) {
        try {
            if (!SodaDetailsActivity.this.isFinishing()) {
                getSupportActionBar().setTitle(dto.getName());

                ((Toolbar) findViewById(R.id.toolbar)).setTitle(dto.getName());
                ((CollapsingToolbarLayout) findViewById(R.id.toolbar_layout)).setTitle(dto.getName());
                sodaDescriptionTextView.setText(dto.getDescription());
                Glide.with(SodaDetailsActivity.this)
                        .load(dto.getBackgroundUrl())
                        .into(backgroundImageView);
            }
        } catch (Exception e) {
            Toast.makeText(SodaDetailsActivity.this, "Unable to load soda details", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void startDetailsAnimation() {
        progressBar.animate().alpha(0f).setDuration(ANIM_DURATION);
        viewInterfaceLocker.animate().alpha(0f).setDuration(ANIM_DURATION);

        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.setAlpha(0f);
        appBarLayout.setVisibility(View.VISIBLE);
        appBarLayout.animate().alpha(1f).setDuration(ANIM_DURATION).start();

        View nestedScrollView = findViewById(R.id.nestedScrollView);
        nestedScrollView.setAlpha(0f);
        nestedScrollView.setVisibility(View.VISIBLE);
        nestedScrollView.animate().alpha(1f).setDuration(ANIM_DURATION).start();
    }

    @Override
    public void showLoadingSpinner() {
        progressBar.setVisibility(View.VISIBLE);
        viewInterfaceLocker.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingSpinner() {
        progressBar.setVisibility(View.GONE);
        viewInterfaceLocker.setVisibility(View.GONE);
    }
}
