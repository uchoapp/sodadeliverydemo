package com.example.sodadelivery.modules.details;

import com.example.sodadelivery.domain.dtos.SodaDto;
import com.example.sodadelivery.domain.dtos.SodaPurchaseDto;
import com.example.sodadelivery.domain.usecases.PurchaseSodaUseCase;
import com.example.sodadelivery.domain.usecases.ShowSodaDetailsUseCase;
import com.example.sodadelivery.domain.utils.Callback;

public class SodaDetailsPresenter implements SodaDetailsContract.Presenter {
    private final SodaDetailsContract.View view;
    private final PurchaseSodaUseCase purchaseSodaUseCase;
    private final ShowSodaDetailsUseCase showSodaDetailsUseCase;

    public SodaDetailsPresenter(ShowSodaDetailsUseCase showSodaDetailsUseCase, PurchaseSodaUseCase purchaseSodaUseCase, SodaDetailsContract.View view) {
        this.view = view;
        this.purchaseSodaUseCase = purchaseSodaUseCase;
        this.showSodaDetailsUseCase = showSodaDetailsUseCase;
    }

    @Override
    public void loadSoda(int sodaId) {
        view.showLoadingSpinner();
        showSodaDetailsUseCase.execute(sodaId, new Callback<SodaDto>() {
            @Override
            public void call(SodaDto sodaDto) {
                view.hideLoadingSpinner();
                view.showDetails(sodaDto);
                view.startDetailsAnimation();
            }
        });
    }

    @Override
    public void purchaseSoda(int sodaId) {
        purchaseSodaUseCase.execute(sodaId, new Callback<SodaPurchaseDto>() {
            @Override
            public void call(SodaPurchaseDto sodaPurchaseDto) {
                view.showPurchaseFeedback(sodaPurchaseDto.getResult());
            }
        });
    }
}
