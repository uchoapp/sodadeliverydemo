package com.example.sodadelivery.modules.list;

import com.example.sodadelivery.BasePresenter;
import com.example.sodadelivery.BaseView;
import com.example.sodadelivery.domain.models.Soda;

import java.util.List;

public interface SodaListContract {

    interface Router {
        void navigateToSodaDetails(int sodaId);
    }

    interface View extends BaseView {
        void showSodas(List<Soda> sodas);
    }

    interface Presenter extends BasePresenter {
        void loadSodas();
    }
}