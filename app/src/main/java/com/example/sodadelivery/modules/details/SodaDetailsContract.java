package com.example.sodadelivery.modules.details;

import com.example.sodadelivery.BasePresenter;
import com.example.sodadelivery.BaseView;
import com.example.sodadelivery.domain.dtos.SodaDto;

public interface SodaDetailsContract {
    interface View extends BaseView {
        void showDetails(SodaDto sodaDto);
        void startDetailsAnimation();
        void showPurchaseFeedback(boolean purchaseResult);
    }

    interface Presenter extends BasePresenter {
        void loadSoda(int sodaId);
        void purchaseSoda(int sodaId);
    }
}
