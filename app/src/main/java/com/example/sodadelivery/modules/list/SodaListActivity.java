package com.example.sodadelivery.modules.list;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.example.sodadelivery.Injector;
import com.example.sodadelivery.R;
import com.example.sodadelivery.domain.models.Soda;
import com.example.sodadelivery.modules.details.SodaDetailsActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class SodaListActivity extends AppCompatActivity implements SodaListContract.View, SodaListContract.Router {
    private static final String LOG_TAG = SodaListActivity.class.getSimpleName();
    private static final String SODAS_KEY = "sodas_key";
    private List<Soda> sodas;
    private ListView sodaList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soda_list);
        sodaList = (ListView) findViewById(R.id.soda_list);

        if (savedInstanceState == null) {
            Log.d(LOG_TAG, "onCreate");
            SodaListContract.Presenter presenter = Injector.getSodaListPresenter(this, this);
            presenter.loadSodas();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.e(LOG_TAG, "\t onSaveInstanceState");
        outState.putString(SODAS_KEY, new Gson().toJson(sodas));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            Log.e(LOG_TAG, "\t onRestoreInstanceState");
            String jsonMyObject = savedInstanceState.getString(SODAS_KEY);
            Type type = new TypeToken<List<Soda>>() {}.getType();
            sodas = new Gson().fromJson(jsonMyObject, type);
            showSodas(sodas);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void showSodas(List<Soda> sodas) {
        this.sodas = sodas;
        sodaList.setAdapter(new SodaListAdapter(this, sodas));
    }

    @Override
    public void showLoadingSpinner() {
        findViewById(R.id.spinner).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingSpinner() {
        findViewById(R.id.spinner).setVisibility(View.GONE);
    }

    @Override
    public void navigateToSodaDetails(int sodaId) {
        Intent intent = new Intent(this, SodaDetailsActivity.class);
        intent.putExtra(SodaDetailsActivity.EXTRA_SODA_ID, sodaId);
        startActivity(intent);
    }
}
