package com.example.sodadelivery.modules.list;

import com.example.sodadelivery.domain.models.Soda;
import com.example.sodadelivery.domain.usecases.RetrieveAllSodasUseCase;
import com.example.sodadelivery.domain.utils.Callback;

import java.util.List;

public class SodaListPresenter implements SodaListContract.Presenter {

    private final SodaListContract.View view;
    private final RetrieveAllSodasUseCase retrieveAllSodasUseCase;

    public SodaListPresenter(RetrieveAllSodasUseCase retrieveAllSodasUseCase, SodaListContract.View view) {
        this.retrieveAllSodasUseCase = retrieveAllSodasUseCase;
        this.view = view;
    }

    @Override
    public void loadSodas() {
        view.showLoadingSpinner();
        retrieveAllSodasUseCase.execute(new Callback<List<Soda>>() {
            @Override
            public void call(List<Soda> sodas) {
                view.hideLoadingSpinner();
                view.showSodas(sodas);
            }
        });
    }
}