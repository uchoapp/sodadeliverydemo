package com.example.sodadelivery.modules.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.sodadelivery.R;
import com.example.sodadelivery.domain.models.Soda;

import java.util.List;

class SodaListAdapter extends BaseAdapter {
    private static final int ANIM_END = 1;
    private static final int ANIM_START = 0;
    private static final int ANIM_DURATION = 500;

    private Context context;
    private List<Soda> sodas;
    private LayoutInflater inflater;

    SodaListAdapter(Context context, List<Soda> sodas) {
        this.sodas = sodas;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    public int getCount() {
        return sodas.size();
    }

    public Soda getItem(int position) {
        return this.sodas.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;

        if (view == null) {
            view = inflater.inflate(R.layout.item_soda, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final Soda soda = sodas.get(position);
        viewHolder.sodaName.setText(soda.getName());
        viewHolder.sodaPrice.setText(context.getString(R.string.price_prefix) + " " + String.valueOf(soda.getPrice()));
        viewHolder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SodaListActivity) context).navigateToSodaDetails(soda.getId());
            }
        });

        Glide.with(context)
                .load(soda.getThumbUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .animate(android.R.anim.fade_in)
                .fitCenter()
                .into(viewHolder.sodaIcon);

        Animation fadeIn = new AlphaAnimation(ANIM_START, ANIM_END);
        fadeIn.setDuration(ANIM_DURATION);
        view.startAnimation(fadeIn);
        return view;
    }

    private class ViewHolder {
        Button btnDetail;
        TextView sodaName;
        TextView sodaPrice;
        ImageView sodaIcon;

        ViewHolder(View v) {
            sodaName = (TextView) v.findViewById(R.id.soda_name);
            sodaPrice = (TextView) v.findViewById(R.id.soda_price);
            sodaIcon = (ImageView) v.findViewById(R.id.soda_icon);
            btnDetail = (Button) v.findViewById(R.id.btn_detail);
        }
    }
}

