package com.example.sodadelivery.domain.models;

public class Soda {
    private int id;
    private String name;
    private String description;
    private String thumbUrl;
    private String backgroundUrl;
    private double price;

    private Soda() { }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public String getBackgroundUrl() { return backgroundUrl; }

    public double getPrice() {
        return price;
    }

    public static class Builder {
        private Soda soda;

        public static Builder newBuilder() {
            return new Builder();
        }

        private Builder() {
            this.soda = new Soda();
        }

        public Builder withId(int id) {
            soda.id = id;
            return this;
        }

        public Builder withName(String name) {
            soda.name = name;
            return this;
        }

        public Builder withDescription(String description) {
            soda.description = description;
            return this;
        }

        public Builder withThumbUrl(String thumbUrl) {
            soda.thumbUrl = thumbUrl;
            return this;
        }

        public Builder withBackgroundUrl(String backgroundUrl) {
            soda.backgroundUrl = backgroundUrl;
            return this;
        }

        public Builder withPrice(double price) {
            soda.price = price;
            return this;
        }

        public Soda build() {
            return soda;
        }
    }
}
