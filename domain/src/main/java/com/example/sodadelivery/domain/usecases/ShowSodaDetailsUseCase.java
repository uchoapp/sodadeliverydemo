package com.example.sodadelivery.domain.usecases;

import com.example.sodadelivery.domain.dtos.SodaDto;
import com.example.sodadelivery.domain.models.Soda;
import com.example.sodadelivery.domain.utils.Callback;

public interface ShowSodaDetailsUseCase {
    void execute(int sodaId, final Callback<SodaDto> callback);
}