package com.example.sodadelivery.domain.mappers;

import com.example.sodadelivery.domain.dtos.SodaDto;
import com.example.sodadelivery.domain.models.Soda;

public class SodaMapper {

    public static Soda mapToModel(SodaDto sodaDto) {
        return Soda.Builder.newBuilder()
                .withId(sodaDto.getId())
                .withName(sodaDto.getName())
                .withDescription(sodaDto.getDescription())
                .withPrice(sodaDto.getPrice())
                .withBackgroundUrl(sodaDto.getBackgroundUrl())
                .withThumbUrl(sodaDto.getThumbUrl())
                .build();
    }

}
