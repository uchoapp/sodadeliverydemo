package com.example.sodadelivery.domain.usecases;

import com.example.sodadelivery.domain.dtos.SodaPurchaseDto;
import com.example.sodadelivery.domain.utils.Callback;

public interface PurchaseSodaUseCase {
    void execute(int sodaId, final Callback<SodaPurchaseDto> callback);
}