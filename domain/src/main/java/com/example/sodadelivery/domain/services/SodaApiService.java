package com.example.sodadelivery.domain.services;

import com.example.sodadelivery.domain.dtos.SodaDto;
import com.example.sodadelivery.domain.dtos.SodaPurchaseDto;
import com.example.sodadelivery.domain.utils.Callback;

import java.util.List;

public interface SodaApiService {
    void retrieveSodas(Callback<List<SodaDto>> callback);
    void retrieveSoda(int sodaId, Callback<SodaDto> callback);
    void purchaseSoda(int sodaId, Callback<SodaPurchaseDto> callback);
}