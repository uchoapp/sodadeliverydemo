package com.example.sodadelivery.domain.dtos;

public class SodaDto {
    private final int id;
    private final String name;
    private final String description;
    private final String thumbUrl;
    private final String backgroundUrl;
    private final double price;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public String getBackgroundUrl() { return backgroundUrl; }

    public double getPrice() {
        return price;
    }

    public SodaDto(int id, String name, String description, String thumbUrl, String backgroundUrl, double price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.thumbUrl = thumbUrl;
        this.backgroundUrl = backgroundUrl;
        this.price = price;
    }
}