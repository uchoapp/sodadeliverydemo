package com.example.sodadelivery.domain.utils;

public interface Callback<T> {
    void call(T value);
}
