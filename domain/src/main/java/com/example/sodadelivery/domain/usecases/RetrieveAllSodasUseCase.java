package com.example.sodadelivery.domain.usecases;

import com.example.sodadelivery.domain.models.Soda;
import com.example.sodadelivery.domain.utils.Callback;

import java.util.List;

public interface RetrieveAllSodasUseCase {
    void execute(final Callback<List<Soda>> callback);
}