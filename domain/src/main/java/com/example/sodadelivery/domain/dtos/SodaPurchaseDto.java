package com.example.sodadelivery.domain.dtos;

public class SodaPurchaseDto {
    private final boolean result;

    public SodaPurchaseDto(boolean result) {
        this.result = result;
    }

    public boolean getResult() {
        return result;
    }
}
