package com.example.library.usecases;

import com.example.library.ExecutionScheduler;
import com.example.sodadelivery.domain.dtos.SodaPurchaseDto;
import com.example.sodadelivery.domain.services.SodaApiService;
import com.example.sodadelivery.domain.usecases.PurchaseSodaUseCase;
import com.example.sodadelivery.domain.utils.Callback;

public class PurchaseSodaUseCaseImpl implements PurchaseSodaUseCase {

    private final SodaApiService sodaApiService;
    private final ExecutionScheduler executionScheduler;

    public PurchaseSodaUseCaseImpl(ExecutionScheduler executionScheduler, SodaApiService sodaApiService) {
        this.executionScheduler = executionScheduler;
        this.sodaApiService = sodaApiService;
    }

    @Override
    public void execute(final int sodaId, final Callback<SodaPurchaseDto> callback) {
        executionScheduler.getBackgroundExecutor().post(new Runnable() {
            public void run() {
                sodaApiService.purchaseSoda(sodaId, new Callback<SodaPurchaseDto>() {
                    @Override
                    public void call(final SodaPurchaseDto sodaPurchaseDto) {
                        executionScheduler.getMainExecutor().post(new Runnable() {
                            @Override
                            public void run() {
                                callback.call(sodaPurchaseDto);
                            }
                        });
                    }
                });
            }
        });
    }
}
