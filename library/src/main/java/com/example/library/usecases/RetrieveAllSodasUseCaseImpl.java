package com.example.library.usecases;

import com.example.library.ExecutionScheduler;
import com.example.sodadelivery.domain.dtos.SodaDto;
import com.example.sodadelivery.domain.mappers.SodaMapper;
import com.example.sodadelivery.domain.models.Soda;
import com.example.sodadelivery.domain.services.SodaApiService;
import com.example.sodadelivery.domain.usecases.RetrieveAllSodasUseCase;
import com.example.sodadelivery.domain.utils.Callback;

import java.util.ArrayList;
import java.util.List;

public class RetrieveAllSodasUseCaseImpl implements RetrieveAllSodasUseCase {

    private final SodaApiService sodaApiService;
    private final ExecutionScheduler executionScheduler;

    public RetrieveAllSodasUseCaseImpl(ExecutionScheduler executionScheduler, SodaApiService sodaApiService) {
        this.executionScheduler = executionScheduler;
        this.sodaApiService = sodaApiService;
    }

    @Override
    public void execute(final Callback<List<Soda>> callback) {
        executionScheduler.getBackgroundExecutor().post(new Runnable() {
            public void run() {
                sodaApiService.retrieveSodas(new Callback<List<SodaDto>>() {
                    @Override
                    public void call(List<SodaDto> response) {
                        final ArrayList<Soda> sodas = new ArrayList<>();
                        for (SodaDto dto : response) {
                            sodas.add(SodaMapper.mapToModel(dto));
                        }

                        executionScheduler.getMainExecutor().post(new Runnable() {
                            @Override
                            public void run() {
                                callback.call(sodas);
                            }
                        });
                    }
                });
            }
        });
    }
}
