package com.example.library.usecases;

import com.example.library.ExecutionScheduler;
import com.example.sodadelivery.domain.dtos.SodaDto;
import com.example.sodadelivery.domain.services.SodaApiService;
import com.example.sodadelivery.domain.usecases.ShowSodaDetailsUseCase;
import com.example.sodadelivery.domain.utils.Callback;

public class ShowSodaDetailsUseCaseImpl implements ShowSodaDetailsUseCase {

    private final SodaApiService sodaApiService;
    private final ExecutionScheduler executionScheduler;

    public ShowSodaDetailsUseCaseImpl(ExecutionScheduler executionScheduler, SodaApiService sodaApiService) {
        this.executionScheduler = executionScheduler;
        this.sodaApiService = sodaApiService;
    }

    @Override
    public void execute(final int sodaId, final Callback<SodaDto> callback) {
        executionScheduler.getBackgroundExecutor().post(new Runnable() {
            public void run() {
                sodaApiService.retrieveSoda(sodaId, new Callback<SodaDto>() {
                    @Override
                    public void call(final SodaDto dto) {
                        executionScheduler.getMainExecutor().post(new Runnable() {
                            @Override
                            public void run() {
                                callback.call(dto);
                            }
                        });
                    }
                });
            }
        });
    }
}
