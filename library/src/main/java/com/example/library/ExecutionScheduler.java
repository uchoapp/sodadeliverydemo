package com.example.library;

import android.os.Handler;

public interface ExecutionScheduler {
    Handler getMainExecutor();
    Handler getBackgroundExecutor();
}
