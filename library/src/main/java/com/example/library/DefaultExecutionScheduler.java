package com.example.library;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

public class DefaultExecutionScheduler implements ExecutionScheduler {

    private static int counter = 1;

    @Override
    public Handler getMainExecutor() {
        return new Handler(Looper.getMainLooper());
    }

    @Override
    public Handler getBackgroundExecutor() {
        HandlerThread handlerThread = new HandlerThread("HandlerThread-" + counter);
        handlerThread.start();
        counter++;
        return new Handler(handlerThread.getLooper());
    }
}
