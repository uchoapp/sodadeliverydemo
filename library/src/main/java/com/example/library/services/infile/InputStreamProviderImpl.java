package com.example.library.services.infile;

import android.content.Context;
import android.support.annotation.RawRes;

import com.example.library.services.infile.InputStreamProvider;

import java.io.InputStream;

public class InputStreamProviderImpl implements InputStreamProvider {

    private Context context;

    public InputStreamProviderImpl(Context context) {
        this.context = context;
    }

    @Override
    public InputStream getInputStream(@RawRes int resId) {
        return context.getResources().openRawResource(resId);
    }
}
