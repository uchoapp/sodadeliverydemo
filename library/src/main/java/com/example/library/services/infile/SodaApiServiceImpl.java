package com.example.library.services.infile;

import com.example.library.ExecutionScheduler;
import com.example.library.R;
import com.example.sodadelivery.domain.dtos.SodaDto;
import com.example.sodadelivery.domain.dtos.SodaPurchaseDto;
import com.example.sodadelivery.domain.services.SodaApiService;
import com.example.sodadelivery.domain.utils.Callback;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SodaApiServiceImpl implements SodaApiService {

    private final InputStreamProvider inputStreamProvider;
    private final ExecutionScheduler executionScheduler;
    private List<SodaDto> data;

    public SodaApiServiceImpl(InputStreamProvider inputStreamProvider, ExecutionScheduler executionScheduler) {
        this.inputStreamProvider = inputStreamProvider;
        this.executionScheduler = executionScheduler;
    }

    private void loadData() {
        data = new ArrayList<>();

        try {
            JsonObject root = readInputStreamAsJsonObject(inputStreamProvider.getInputStream(R.raw.sodas));
            JsonArray productList = root.getAsJsonArray("productList");

            for (int i = 0; i < productList.size(); i++) {
                JsonObject item = productList.get(i).getAsJsonObject();
                data.add(new SodaDto(item.get("id").getAsInt(),
                        item.get("name").getAsString(),
                        item.get("description").getAsString(),
                        item.get("thumbUrl").getAsString(),
                        item.get("backgroundUrl").getAsString(),
                        item.get("price").getAsDouble()));
            }
        } catch (Exception e) {
        }
    }

    private JsonObject readInputStreamAsJsonObject(InputStream inputStream) throws UnsupportedEncodingException {
        Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        return new JsonParser().parse(reader).getAsJsonObject();
    }

    @Override
    public void retrieveSodas(final Callback<List<SodaDto>> callback) {
        executionScheduler.getBackgroundExecutor().post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (data == null) {
                        Thread.sleep(new Random().nextInt(500));
                        loadData();
                    }

                    executionScheduler.getMainExecutor().post(new Runnable() {
                        @Override
                        public void run() {
                            callback.call(data);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void retrieveSoda(final int sodaId, final Callback<SodaDto> callback) {
        executionScheduler.getBackgroundExecutor().post(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(new Random().nextInt(500));
                    executionScheduler.getMainExecutor().post(new Runnable() {
                        @Override
                        public void run() {
                            SodaDto sodaDto = null;
                            for (SodaDto dto : data) {
                                if (dto.getId() == sodaId) {
                                    sodaDto = dto;
                                    break;
                                }
                            }
                            callback.call(sodaDto);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void purchaseSoda(int sodaId, final Callback<SodaPurchaseDto> callback) {
        executionScheduler.getBackgroundExecutor().post(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(new Random().nextInt(500));

                    final SodaPurchaseDto purchaseDto = new SodaPurchaseDto(new Random().nextBoolean());

                    executionScheduler.getMainExecutor().post(new Runnable() {
                        @Override
                        public void run() {
                            callback.call(purchaseDto);
                        }
                    });
                } catch (Exception e) {
                }
            }
        });
    }
}