package com.example.library.services.infile;

import android.support.annotation.RawRes;

import java.io.InputStream;

public interface InputStreamProvider {
    InputStream getInputStream(@RawRes int resId);
}
